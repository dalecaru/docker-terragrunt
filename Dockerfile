ARG TERRAFORM_VERSION=0.11.11
FROM hashicorp/terraform:$TERRAFORM_VERSION

ARG TERRAGRUNT_VERSION=0.17.2
ADD https://github.com/gruntwork-io/terragrunt/releases/download/v$TERRAGRUNT_VERSION/terragrunt_linux_amd64 /bin/terragrunt
RUN chmod +x /bin/terragrunt

FROM node:9-alpine
COPY --from=0 bin/terraform /bin/
COPY --from=0 bin/terragrunt /bin/

RUN npm install terraform-ecs-plan-checker -g

RUN apk --no-cache add \
  build-base \
  ruby-dev \
  ruby-bundler \
  ruby-json \
  diffutils

RUN gem install --no-document --no-ri terraform_landscape

CMD [ "terragrunt" ]
