# Terragrunt Docker Container
### Usage
This repository automatically builds containers for using the [`terragrunt`](https://github.com/gruntwork-io/terragrunt) command line program.

You can use it with the following:
```shell
docker run -i -t registry.gitlab.com/dalecaru/docker-terragrunt <command>
```

For example:
Initialize the plugins (You will only need to do this once)

```shell
docker run -i -t -v $(pwd):/app/ -w /app/ registry.gitlab.io/dalecaru/docker-terragrunt terragrunt init
```


```shell
docker run -i -t -v $(pwd):/app/ -w /app/ registry.gitlab.io/dalecaru/docker-terragrunt terragrunt plan
```
